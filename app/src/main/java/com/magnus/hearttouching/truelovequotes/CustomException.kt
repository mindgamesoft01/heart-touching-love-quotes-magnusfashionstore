package com.magnus.hearttouching.truelovequotes

class CustomException(message: String) : Exception(message)
